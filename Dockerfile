FROM node:18.19.0-alpine3.19

# Create server directory
RUN apk update &&\
    apk add git

# Install server dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY package*.json ./
COPY . .
RUN npm install &&\
    npm update
# If you are building your code for production
# RUN npm ci --only=production

# EXPOSE and BIND port
EXPOSE 8100 9802

#WORKDIR /app

CMD [ "npm", "start" ]
