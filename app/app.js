const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");

const config = require("./lib/config");
const redis = require("./lib/redis");

const routes = require("./route");
const logger = require("./lib/logger");

exports.AfConnectOutbox = function() {
  this.app = undefined;
  this.server = undefined;
  this.init = async function() {
    this.app = express();
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.app.use(logger.middleware);
    this.app.use(routes);

    this.server = http.createServer(this.app);
  };
  this.start = async function() {
    await redis.init();
    await new Promise((resolve, reject) => {
      this.server.listen(config.port, () => {
        console.log(`AF Connect Outbox server running on port ${config.port}`);
        return resolve(this);
      });
    });
  };
  this.stop = async function() {
    if (this.server === undefined) {
      return this;
    }

    await redis.quit();
    await new Promise((resolve, reject) => {
      this.server.close(() => {
        console.log("AF Connect Outbox server terminated successfully");
        return resolve();
      });
    });
  };
};
