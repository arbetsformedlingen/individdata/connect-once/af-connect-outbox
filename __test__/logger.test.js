describe("logger", () => {
  test("middleware", done => {
    const { middleware } = require("../app/lib/logger");
    let req = {};
    let res = {};
    middleware(req, res, () => {
      return done();
    });
  });

  test("format", async () => {
    const { format } = require("../app/lib/logger");
    expect(format({ hello: "world" })).toBe('{"hello":"world"}');
  });
});
